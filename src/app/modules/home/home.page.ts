import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-home",
  templateUrl: "./home.page.html",
  styleUrls: ["./home.page.scss"],
})
export class HomePage implements OnInit {
  constructor() {}

  async ngOnInit() {
    console.log("ngOnInit HomePage");
  }

  goToNotif() {
    console.log("goToNotif");
  }
}
